# Kerberos

This is the kerberos image used by the CRI.
It provide a MIT kerberos V5 backed by an LDAP.
This image is a fork of  gcavalcante8808/krb5-server.

## Usage

```
[libdefaults]
	default_realm = YOURREALM.FQDN

[realms]
	YOURREALM.FQDN = {
		kdc = localhost
		admin_server = localhost
	}
```
```
kinit admin/admin@YOURREALM.FQDN # Will prompt for the password provided or the generated.
klist
```
**The Default Kadmin policy allows all members inside /admin policy to anything in your kerberos database(default to * perm); if you need a more simple user that will be used to addprincipals and generate keytabs you can create users with /service policy (defaults to aci perm)**.

## Environment variables

### Mandatory

* `KRB5_REALM`: Your KRB5 REALM name in Upper Case and DNS format, like EXAMPLE.COM;
* `KRB5_KDC`: Your KRB5 KDC Address. It's recommended that you use a TXT Dns entry, but you can use localhost for a simple installation (if you use localhost you can't setup the KDC slaves later ...);

### Optional

* `KRB5_ADMINSERVER`: If not provided will be the same value that was provided for `$KRB5_KDC`;
* `KRB5_ADMIN_PASSWORD`: KDB and **admin** password for the database. If you don't provide this value, one will be created and printed in the first time that container is started; **write down this password, it is necessary to login with kadmin and unblock the kdb files**.
* `KRB5_ADMIN_PASSWORD_FILE`: If not empty sets the content of `KRB5_ADMIN_PASSWORD` to the content of the provided file.
* `KRB5_MASTER_PASSWORD`: Kerberos master password for the LDAP, default to `admin_krb`, if not specified generate one randomly and print it on stdout.
* `KRB5_MASTER_PASSWORD_FILE`: If not empty sets the content of `KRB5_MASTER_PASSWORD` to the content of the provided file.
* `KADMIN_LAUNCH`: Set to `1` if kadmin should be launched instead of the kdc, `0` otherwise. This variable is set by default to `0`. This variable take effects *ONLY* if an empty command is given.
* `LDAP_DN`:  The base LDAP DN used by kerberos, default to `dc=example,dc=org`. This variable is not actually used directly but change the default value of other DN variables.
* `LDAP_KRB5_CONTAINER_DN`: The LDAP DN actually used by kerberos, default to `cn=krbContainer,$LDAP_DN`.
* `LDAP_ADMIN_DN`: The admin LDAP DN that is used to create entries for kerberos in the LDAP, default to `cn=admin,$LDAP_DN`.
* `LDAP_ADMIN_PASSWORD`: The password of `LDAP_ADMIN_DN`, default to `admin`.
* `LDAP_ADMIN_PASSWORD_FILE`: If not empty sets the content of `LDAP_ADMIN_PASSWORD` to the content of the provided file.
* `LDAP_KDC_DN`: The kdc LDAP DN that is used to create entries for kerberos in the LDAP, default to `cn=admin,$LDAP_DN`.
* `LDAP_KDC_PASSWORD`: The password of `LDAP_KDC_DN`, default to `admin`.
* `LDAP_KDC_PASSWORD_FILE`: If not empty sets the content of `LDAP_KDC_PASSWORD` to the content of the provided file.
* `LDAP_URI`: The URI of the LDAP.
* `LDAP_SETUP`:  Set to `1` if the underlying LDAP should be setuped, `0` otherwise. This variable is set by default to `1`. The setup is **ignored** if the underlying LDAP is already setuped with the specified DN.


### Note About Low Entropy and Kerberos Database Creation
If your container won't start properly and show a message like "Loading random data" for couple minutes indicates that the system don't have enough entropy avail to provide a secure cryptographyc loop to the program.

In this case you can use rngd (will be necessary to restart the container after this):

```
/sbin/rngd
```
You can use havaged as well, as we can see in the digitalOcean tutorial:
https://www.digitalocean.com/community/tutorials/how-to-setup-additional-entropy-for-cloud-servers-using-haveged

After this, you just need to restart your container and it is g-n-go.

## Other Information
This container uses the Krb5-Server provided by the Alpine Team. Take a look at the alpine site to verify available versions of the package.

For more information on how to configure the clients or even the server take a loot at the MIT Krb5 Documentation.
